JSON objects must conform to the following standard to be included in this repository:

- Pretty-printed using one tab for each indentation level

The reason for this strict requirement is to help avoid as many merge conflicts as possible.
Commits that do not follow this requirement will be rejected by the CI/CD system.
